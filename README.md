Read.me

NaOmi v.2.0
A Python based program for clustering the feedbacks data collected from Happyfresh m-web and application. The program clusters data in cluster and sub-cluster.

Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

Prerequisites
Before installing or forking the program, you need these programs/softwares below:
- Python version 2.7 or above  (64 bit for better compile)
- PyCharm Version: 2019.3.1; Build: 193.5662.61 or above
- Git (2.25.0) 32-bit version or above (can use the 64 bit version

Installing
1. Open your terminal, then type: cd (your desired file location for your repositories), then press enter.
2. type: git init, then press enter
3. Now you have made your local repositories, now you need to clone the gitlab repositories to download (clone) the program. For that you need to type: git clone https://gitlab.com/romihadiyan/naomi-ver.2.0.git then press enter
4. wait until the cloning process finished. 
5. After it’s finished its cloning process, the folder you just cloned must’ve appeared on your repositories and ready to use.

Usage
1. NaOmi v.2.0 is similar with the previous version.
2. In NaOmi v.2.0, there will be 2 sub-program for the clustering and Sub-Clustering.
3. You need to use the fix or template for the table (csv) file that has been used before (from previous quarter) The Feedback/Message has to be on Column has to be named "Cluster" and "Subcluster" (Case Sensitive) "M" and Subcluster/Cluster on Column "Q" on Your Sheet.
4. Open the program file you want to use (Cluster/Sub-Cluster) file.
5. The window will appear once you click.
6. There will be some clusters/sub-clusters (depend on the program you open).
7. You can add or delete the clusters/sub-clusters, after you add or delete the cluster/subcluster it will appear or deleted from the cluster/sublucster list (you have to delete the default words on the input field).
8. You can load the file you want to process in csv by pressing "Open File" then Finder Window will appear for you to choose the csv,
9. If you’re successfully load the file, a pop up message will appear.
10. Wait until the process is done (depends on the size of file) when a finder screen appear.
11. After the process is done, you’re required to select the directory you want it to be saved.
12. That’s all fingercrossed.


Built With
* Python - Programming language used
* Pycharm - Application builder and compiler.


Versioning
We use SemVer for versioning. For the versions available, see the tags on this repository.

Authors
* Naufal Irbahhana - Initial work - UX Researcher Intern
* Romi Hadiyan Aji Witjaksono - Maintaining and updating the program - UX Researcher Intern
